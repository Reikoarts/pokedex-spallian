export const getPokemon = async (pageParam) => {
    const response = await fetch(`https://pokeapi.co/api/v2/pokemon/?limit=50&offset=${pageParam}`);
    const data = await response.json();
    return data.results;
}


export const getPokemonType = async (name) => {
    const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${name}`);
    const data = await response.json();
    const pokemonType = data.types.map((pokemon) => pokemon.type.name);
    return pokemonType;
}



export const getOnePokemon = async (name) => {
    const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${name}`);
    const data = await response.json();
    return data;
}