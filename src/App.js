import React from 'react';
import Index from './screen/Index';

import { QueryClient, QueryClientProvider } from 'react-query';

const App = () => {

  return (
  <Index client={QueryClient}/>
  );
};
export default App;