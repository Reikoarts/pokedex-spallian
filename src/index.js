import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import Index from './screen/Index';
import ErrorPage from './screen/ErrorPage';
import Pokemon from './screen/Pokemon';

import './styles.css';
import Layout from './componants/Layout';
import AnimatedRoutes from './componants/AnimatedRoutes';

const root = ReactDOM.createRoot(document.getElementById('root'));

const queryClient = new QueryClient();


root.render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <Router>
        <AnimatedRoutes/>
      </Router>
    </QueryClientProvider>
  </React.StrictMode>
);