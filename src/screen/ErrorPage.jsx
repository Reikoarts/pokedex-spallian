import React from 'react'
import { useRouteError } from "react-router-dom";
import {motion} from 'framer-motion'

const ErrorPage = () => {
  const error = useRouteError();
  console.error(error);
  return (
    <motion.div id="error-page" className='error_container' 
    initial={{width: 0, opacity: 0}} 
    animate={{width: "100vw", opacity: 100}} 
    exit={{x: window.innerWidth, transition:{ duration: 0.1}}}>
      <h1>Oops!</h1>
      <p>On dirait que cette page n'existe pas 😢</p>
      <p>
        <i>{error.statusText || error.message}</i>
      </p>
    </motion.div>
  );
}

export default ErrorPage