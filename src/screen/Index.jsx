import React from 'react';
import { useInfiniteQuery } from 'react-query';
import { getPokemon, getPokemonSprite } from "../utils/api"
import Title from '../componants/Title';
import Card from '../componants/Card';
import {motion} from 'framer-motion'
import SearchBar from '../componants/SearchBar';


const Index = () => {

  const queryKey = ['Pokemon']
  const {isLoading, data, error, fetchNextPage, hasNextPage, isFetchingNextPage} = useInfiniteQuery(queryKey, ({pageParam}) => 
  getPokemon(pageParam), {
    getNextPageParam: (actualPage, allPages) => {
      let totalPage = Object.keys(actualPage).length * Object.keys(allPages).length
      return totalPage
    },
    staleTime: 60000,
  });


  return (
    <motion.div className='container' 
    initial={{width: 0, opacity: 0}} 
    animate={{width: "100vw", opacity: 100}} 
    exit={{x: window.innerWidth, transition:{ duration: 0.1}}}>
      <Title title='Pokédex' />
      <SearchBar data={data}/>
      {isLoading==false ? data.pages.map((page, index) => (
        <div key={index} className='home_container'>
          {page.map((pokemon, index) => {
            let Id = pokemon.url.split('/')[6];
            return (
              <Card listKey={index} data={pokemon} Id={Id} />
            );
          })}
        </div>
      )) : "Chargement..."}
  
      {hasNextPage && (
        <button onClick={() => fetchNextPage()} disabled={isFetchingNextPage} className='button'>
          {isFetchingNextPage ? 'Chargement...' : 'Page suivante'}
        </button>
      )}
    </motion.div>
  );
}

export default Index;