import React from 'react'
import { useParams } from 'react-router-dom'
import { useQuery } from 'react-query'
import { getOnePokemon } from '../utils/api'
import Title from '../componants/Title'
import LittleTitle from '../componants/LittleTitle'
import { HashLink } from 'react-router-hash-link'
import {motion} from 'framer-motion'

const Pokemon = () => {

  const { pokemonName } = useParams()

  const queryKey = ['Pokemon', pokemonName]
  const { isLoading, data, error } = useQuery(queryKey, () => getOnePokemon(pokemonName), {
    staleTime: 60000,
  });

  console.log('data', data)

  while (!data) {
    return (
      <div>
        Loading...
      </div>
    )
  }

  return (
    <motion.div className='pokemon_container' 
    initial={{width: 0, opacity: 0}} 
    animate={{width: "100vw", opacity: 100}} 
    exit={{x: window.innerWidth, transition:{ duration: 0.1}}}>
      <div className='pokemon_container_left'>
        <img src={data.sprites.front_default} alt={"image de " + pokemonName} />
        <img src={data.sprites.front_shiny} alt={"image de " + pokemonName} />
      </div>
      <div className='pokemon_container_right'>
          <div className='pokemon_info'>
            <Title title={pokemonName} />
            <p>Poids : {data.weight} kg</p>
            <div className='pokemon_type_container'>
            <p>type :</p>
              {isLoading === false ? data.types.map((type, index) => (
                <p key={index} className="pokemon_type">{type.type.name}</p>
              )) : "Chargement..."}
            </div>
          </div>
          <div>
            <LittleTitle text='Statistique de base' />
            <table>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Valeur</th>
                </tr>
              </thead>
              <tbody>
                {data.stats.map((stat, index) => {
                  return (
                    <tr key={index}>
                      <td>{stat.stat.name}</td>
                      <td>{stat.base_stat}</td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </div>
      </div>
      <HashLink to={`/#${pokemonName}`} className='back'>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="64" height="64"><path d="M12 2C17.52 2 22 6.48 22 12C22 17.52 17.52 22 12 22C6.48 22 2 17.52 2 12C2 6.48 6.48 2 12 2ZM12 11V8L8 12L12 16V13H16V11H12Z" fill="rgba(255,255,255,1)"></path></svg>
      </HashLink>
    </motion.div>
  )
}

export default Pokemon