import React from 'react'

const Title = ({title}) => {

  let newTitle = Array.from(title)

  const part1 = newTitle.slice(0, newTitle.length/2)
  const part2 = newTitle.slice(newTitle.length/2, newTitle.length)
  

  return (
    <h1 className='h1'>{part1}<span>{part2}</span></h1>
  )
}

export default Title