import React from 'react'
import { Link } from 'react-router-dom'
import LittleTitle from './LittleTitle'
import {motion} from 'framer-motion'

const Card = ({listKey, data, Id}) => {
    return (
        <Link key={listKey} to={`/pokemon/${data.name}`} className='pokemon_card' id={data.name}>
            <motion.div 
            initial={{opacity: 0}} 
            animate={{opacity: 1}}>
                <img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${Id}.png`} alt={"image de " + data.name} />
                <LittleTitle text={data.name} />
            </motion.div>
        </Link>
    )
}
export default Card