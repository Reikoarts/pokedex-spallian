import React from 'react'

const LittleTitle = ({text}) => {
  return (
    <h2 className='h2'>{text}</h2>
  )
}

export default LittleTitle