import React from 'react'
import { Routes, Route } from 'react-router-dom'
import Layout from './Layout'
import Index from '../screen/Index'
import Pokemon from '../screen/Pokemon'
import ErrorPage from '../screen/ErrorPage'
import { useLocation } from 'react-router-dom'
import { AnimatePresence } from 'framer-motion'

const AnimatedRoutes = () => {
    const location = useLocation()
    return (
        <AnimatePresence>
            <Routes location={location} key={location.pathname}>
                <Route exact path="/" element={<Layout />}>
                    <Route path="/" element={<Index />} />
                    <Route path="/pokemon/:pokemonName" element={<Pokemon />} />
                    <Route path="*" element={<ErrorPage />} />
                </Route>
            </Routes>
        </AnimatePresence>
    )
}

export default AnimatedRoutes