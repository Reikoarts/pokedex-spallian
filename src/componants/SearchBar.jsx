import React from 'react'
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const SearchBar = ({ data }) => {

  const [search, setSearch] = useState('')
  const [AllPokemon, setAllPokemon] = useState([])
  const [filteredPokemon, setFilteredPokemon] = useState([])
  const [isVisible, setIsVisible] = useState("collapse")

  useEffect(() => {
    if (!data) return
    const pages = Array.from(data.pages)
    setAllPokemon(pages.reduce((acc, page) => [...acc, ...page], [])) //concatenation des tableaux de pokémon stocker dans les pages
  }, [data])

  const searchPokemon = () => {
    setFilteredPokemon(AllPokemon.filter((pokemon) => pokemon.name.startsWith(search)))
  }

  return (
    <form className='search_form'>
      <input type="text" placeholder='Rechercher un Pokémon' className='search_bar' onChange={(e) => {
        setSearch(e.target.value)
        searchPokemon()
        setIsVisible("visible")
      }}
      onBlur={()=>{setIsVisible("collapse")}}
      onFocus={()=>{setIsVisible("visible")}} />
      <ul className='search_list' style={{visibility: isVisible}}>
        {filteredPokemon ? filteredPokemon.map((pokemon, index) => {
          return (
            <Link to={`/pokemon/${pokemon.name}`} className='link' key={index}>
              <li key={index} className='search_result'>
                <p>{pokemon.name}</p>
              </li>
            </Link>
          )
        }) : "Chargement..."}
      </ul>
    </form>
  )
}

export default SearchBar