import React from 'react';
import Pokeball from '../assets/img/pokeball.svg';
import { Link } from 'react-router-dom';

const Navigation = () => {
  return (
    <div className='nav'>
      <Link to="/">
        <img src={Pokeball} alt='Dessin de pokéball' className='pokeball_img' />
      </Link>
      <Link to="/" className='link_nav'>
        Pokédex
      </Link>
    </div>
  );
}

export default Navigation;
